---
title: "How Bash Script and Aliases Could Make Your Life Easier"
date: 2020-09-23T00:34:53+07:00
draft: false
tags: ["sysadmin", "bash","script"]
---

# How Bash Script and Aliases Could Make Your Life Easier

![software-terminology-0](/img/bash1.png)

This is story about developer who start in old PHP world where we use mixed php+html+inline css and deploy to shared hosting via FTP (All GUI). Using rename folder for versioning. Once enter terminal and don’t know what was that black screen, and when accidentally edit file with vim and end up by restarting computer. That’s the time where i was living.

Now, i’m accidentally a backend human who always in front of black screen monitor, either its learning something, code a script or just watch and filter logs (mostly still manual).

## How Bash Script Help Me

This one scripting language could help me in my productivity. Bash have its own way in its syntax. and its literally execute cli command like if you have to start many service to start a project you could just create bash script and run it once. just simplyfy it

this is example for `startlemp.sh`.

```
#!/bin/bash
sudo service php7.0-fpm start
sudo service mysql-server start
sudo service nginx start
```

and combine it with alias to your `.profile` file.

```
alias lemp="sh ~/.startlemp.sh"
```

now you could open computer in the morning little youtube for a while and when you want to start your project just open terminal and type :

```
$lemp
```

For someone that have huge WPM will have no problem with type all of that in second, or someone with high end computer will make those lemp stack in startup so they will alive when computer start. But for someone with little WPM, lazy, and don’t have great spec in computer will really helped by bash script.

> don’t compare with docker. Its about the bash :D and back there docker is not hype yet.

## Bash help me enter SSH with the flag

how to create variable with bash? you could use :

```
var1="is var one"
echo $var1
```

and now i want to type `ss back` will go to my backend server without type the ip over and over, I will use something like this file `myssh.sh` .

```
#!/bin/bash
if [ "$1" = "back" ]; then
        server="user-backend@11.11.11.11"
elif [ "$1" = "front" ]; then
        server="user-frontend@22.22.22.22"
else
        echo "server side required"
        exit 1
fi
echo "will execute"
echo "ssh $server"
ssh "$sshcommand"
```

and once gain combine it with alias.

```
alias ss="sh ~/myssh.sh"
```

now for running that script just type :

- `ss back` to ssh to backend server.
- `ss front` to ssh to front end server.

To be able to ssh to server i want with just two word was awesome. Up until now bash and aliases help me a lot. hope this article, bash, and aliases will help you too.
Thanks

> this is backup from my post on [medium](https://medium.com/@primaadipradana/how-bash-script-and-aliases-could-make-your-life-easier-c188a7c0ec) 
