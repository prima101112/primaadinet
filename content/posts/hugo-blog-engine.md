---
title: "Hugo Blog Engine (My Blog Story)"
slug: "Hugo Blog Engine"
date: 2019-10-20T20:29:34+07:00
draft: false
tags: ["blog-engine", "hugo"]
---

# Hugo Blog Engine (How to)

my blog is gone again and this is my new blog using hugo blog engine.
hugo is static blog generator using mardown syntax as formating.
hugo is similar with jekyll but write in golang

i move from medium to wordpress in gke and now i move to gitlab pages.

## Why Static

there is actually story why i using gitlab pages now.

1. there is incident with my google accounts.
2. this is free of storage since we just pay for the domain.
3. this is no different any way. my blog still bad.

## why hugo

why i choose hugo over jekyll or other tools is because i code in golang. and hugo is easy to use not much setup and its faster than jekyll (they claim to be)


## How to setup hugo in our local machine

1. install hugo first
in ubuntu
```
sudo apt-get install hugo
```
another install please follow this link
https://gohugo.io/getting-started/installing/

2. create hugo project
```
hugo new site mysite
```
there is should be a new folder of your site

3. adding some content
```
hugo new posts/my-first-post.md
```
thats will create new content in `post` folder

4. start hugo server with
```
hugo server -D
```

5. open your hugo site in
```
localhost:1313
```
6. all that step could see in this link 
https://gohugo.io/getting-started/quick-start/

i will explain more about how we deploy our site using gitlab pages with gitlab CI/CD as integration and delivery pipeline
