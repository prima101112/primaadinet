---
title: "Mermaidjs in Hugo"
date: 2019-12-03T10:46:42+07:00
draft: false
tags: ["blog-engine", "hugo", "maermaidjs", "tutorial"]
---

# Mermaid Js In Hugo
<script async src="https://unpkg.com/mermaid@8.2.3/dist/mermaid.min.js"></script>

[mermaid js](https://mermaid-js.github.io/mermaid/#/) is the diagram engine with capability to draw diagrams with text. as SRE, sysadmin or someone that everyday cudling with text or terminal, this mermaid js is so much great to combine with markdown to write a blog post. 

Here i am using hugo as my blog engine. its easy since we write a blog post like we create a README.md in a repo. 

> why not jekyll ?

> [my be i just fall to this benchmark](https://forestry.io/blog/hugo-vs-jekyll-benchmark/)

## How to integrate mermaidjs with hugo blog engine

mermaid js is easy to integrate we just need to include mermaid js script to our pages, we could iclude it inside `themes/yourthemes/layouts`. I add that to template that render just for the post pages. `ex: layouts/_default/single.html` add this included script. 
or if you just want to render it in one post just put it on the md file.

```
<script async src="https://unpkg.com/mermaid@8.2.3/dist/mermaid.min.js"></script>
```

read [this](https://www.w3schools.com/tags/att_script_async.asp) if you curious about async attribute

## include the tags

and add the graph code inside it

```
<div class="mermaid">
graph TD
A[Client] --> B[Load Balancer]
B --> C[Server01]
B --> D[Server02]
</div>
```

this code will render like this
{{<mermaid>}}
graph TD
A[Client] --> B[Load Balancer]
B --> C[Server01]
B --> D[Server02]
{{</mermaid>}}

## Add mermaid to our hugo shortcodes

in hugo we have shortcodes. like [instagram](https://gohugo.io/content-management/shortcodes/#instagram) or [youtube](https://gohugo.io/content-management/shortcodes/#youtube). but we could make our own shortcodes

1. Create file in yourworkdir/layouts/shortcodes/mermaid.html
2. fill that file with this 
    ```
    <div class="mermaid">
        {{.Inner}}
    </div>
    ```
3. inner is used so you could write in the markdown like this
    ```
    {{</*mermaid*/>}}
    graph TD
    A[Client] --> B[Load Balancer]
    B --> C[Server01]
    B --> D[Server02]
    {{</*/mermaid*/>}}
    ```


## How good is mermaid js

let say you have this codes
```
graph LR
    A[Hard edge] -->|Link text| B(Round edge)
    B --> C{Decision}
    C -->|One| D[Result one]
    C -->|Two| E[Result two]
    E -->|back to hard| A
```

it will render like this

{{<mermaid>}}
graph LR
    A[Hard edge] -->|Link text| B(Round edge)
    B --> C{Decision}
    C -->|One| D[Result one]
    C -->|Two| E[Result two]
    E -->|back to hard| A
{{</mermaid>}}

and i think thats awesome   
