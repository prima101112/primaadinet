---
title: "Stress Test Cpu and Memory in Linux"
date: 2020-09-23T00:25:15+07:00
draft: false
tags: ["sysadmin", "test"]
---

![software-terminology-0](https://miro.medium.com/max/1260/1*tc_gVessNzY89cC9VYHOzQ.png)

# Stress Test Cpu and Memory in Linux

Have heard chaos-duck ? no? ok.
That’s the name of the script that will destroy and disrupt an stable system. its license by company where i work.

Why disrupt stable system?
Some stable platform need this kind of script to simulate disaster and apply mitigation before the real disaster coming. so yes i create some script, with disaster level like in the (one punch man). stop that and stop this make that connection stop etc *but* 

## i was confuse how do i make CPU or RAM peak.

in the next day i was alone staring the blue sky and i wonder how could i make cpu machine 100% so some guy in the internet told me (search google) i could use stress tool.
how to install it? just do :

```
sudo apt-get install stress
```

### Stress test to CPU

For doing stress test to cpu we need to know how much cpu in our machine so i could get that with :

```
grep -c ^processor /proc/cpuinfo
```

and do the stress test (example with 2 core) and stress test will be done in 120 second.

```
stress --cpu 2 --timeout 120
```

### Stress test to RAM
to stress test memory we need to define how much we will fill up memory and how much is it. if you have 4Gb ram you could fill up 1024M * 4 it will be :

```
stress --vm 4 --vm-bytes 1024M
```

### Stress test could also test IO

i have not try this but the command should be

```
stress --io 4
```

thats it you could simulate the disaster to your app
![software-terminology](https://devhumor.com/content/uploads/images/June2015/1435337389291.jpg)

You could destroy your own computer too. with combine all command

```
stress --cpu 2 --io 4 --vm 2--vm-bytes 1024M --timeout 300s
```

hope this will help you who have a task to test your system
thank you

> this is backup from my post on [medium](https://medium.com/@primaadipradana/stress-test-cpu-and-memory-in-linux-d17bfa5e8887) 

